import { animation } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  anioActual = new Date();
  anio=this.anioActual.getFullYear();

  constructor() { }
  
  ngOnInit(): void {
  }
}

